var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('book_clicked');

redis.on('message', function(channel, message) {
    message = JSON.parse(message);
    console.log(message);
    io.emit(channel, message.data);
});

io.on('connection', (socket) => {
  console.log('Connexion d\'un utilisateur');
  socket.on('disconnect', () => {
    console.log('Utilisateur deconnecté');
  });
});

http.listen(6001, () => {
  console.log('Ecoute sur *:6001');
});
