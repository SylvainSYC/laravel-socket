<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\BookClicked;
use App\Product;
use Illuminate\Support\Facades\Log;

class BookController extends Controller
{
    public function dispatchEvent(Request $request){
      $P = Product::where('id',$request->id)->first();
      event(new BookClicked($P,$request->nickname,$request->from));
      return response()->json(['OK']);
    }
}
