<?php

namespace App\Listeners;

use App\Events\BookClicked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class BookClickedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookClicked  $event
     * @return void
     */
    public function handle(BookClicked $event)
    {
      try{
        Log::info('BookClickedListener');
      }
      catch(\Exception $e){
        Log::info($e->getMessage(). " ".$e->getLine());
        return false;
      }
    }
}
