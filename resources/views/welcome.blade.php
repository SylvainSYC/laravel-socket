<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    </head>
    <body>
      <div class="container">
        <div class="row">
          @foreach($products as $p)
            <div class="col-md-3">
              <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://dummyimage.com/600x400/000/fff" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">{{$p->nom}}</h5>
                  <p class="card-text">Prix: {{$p->prix}} $</p>
                  <a href="#" class="btn-book" id="{{$p->id}}" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
          @endforeach;
        </div>
      </div>
      <div class="modal" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/socket.io-client@2/dist/socket.io.js"></script>
      <script type="text/javascript">
        var socket = io('http://127.0.0.1:6001');
        $(document).ready(function(){
          var nickname = localStorage.getItem('nickname');
          if(!nickname){
            let n = prompt('Quel est votre pseudo?') ;
            localStorage.setItem('nickname',n);
          }
          $('.btn-book').on('click',function(){
            let n = prompt('A qui souhaitez vous recommander ce livre ?');
            $.ajax({
              method: "POST",
              url: "api/book",
              data: { id: $(this).attr('id'), nickname: n, from: localStorage.getItem('nickname') }
            }).done(function( msg ) {
                alert( "Data Saved: " + msg );
              });
          });
          socket.on("book_clicked", function(data){
            if(data.to == localStorage.getItem('nickname')){
              $('#exampleModal .modal-title').html(data.from+' Vous recommande ce livre !');
              $('#exampleModal .modal-body').html(data.book.nom+'</br />Prix : '+data.book.prix);
              $('#exampleModal').modal('show')
            }
          });
        });
        </script>
    </body>
</html>
