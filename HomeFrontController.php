<?php

namespace App\Http\Controllers\Sites\Concession;

use App\Http\Controllers\Controller;
use App\Http\Controllers\sites\FrontController;
use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Repositories\VehiculeRepository;
use App\Repositories\ActualityRepository;

class HomeFrontController extends FrontController
{
    use SEOToolsTrait;



    // public function getLastVehiclesWithType(VehiculeRepository $infos_vehicles,Request $request){

    //   return $infos_vehicles->getLastVehiclesWithType($request->type,3);
    // }

    public function index(VehiculeRepository $infos_vehicles, ActualityRepository $infos_actualities,Request $request) {
          $this->seo()->setTitle($this->concession->nom . ', concessionnaire du groupe Idylcar à ' . $this->concession->ville);
          $this->seo()->setDescription($this->concession->nom . ', concessionnaire du groupe Idylcar à ' . $this->concession->ville);

          $actualities = $infos_actualities->getLastActualities(3,$this->concession->id);

          return view('front.concessionnaire.home')->with([
              'role' =>"concessionnaire",
              'concession' => $this->concession,
              'vehicules' => $infos_vehicles->getTypeOfVehiculesAvailable($this->concession->id),
              'recentVehicles' => $infos_vehicles->getLastVehiclesGroupByCategory($this->concession->id),
          ]);
    }
}
