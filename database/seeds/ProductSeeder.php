<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('product')->insert([
          'nom' => Str::random(50),
          'categorie' => 'Livres',
          'prix'=> random_int(5,100)
      ]);
    }
}
